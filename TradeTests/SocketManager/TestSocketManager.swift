//
//  TestSocketManager.swift
//  TradeTests
//
//  Created by Yoganand Nandakumar on 13/05/2021.
//

import XCTest
@testable import Trade

class TestSocketManager: XCTestCase, SocketManagerProtocol {
    let socket = SocketManager.shared
    var onSuccessReceiveMsg: XCTestExpectation!
    var onDisconnect: XCTestExpectation!
    var onSuccessfullSocketConnection: XCTestExpectation!

    override func setUpWithError() throws {
        socket.delegate = self
    }

    func testSocketConnection() {
        onSuccessfullSocketConnection = self.expectation(description: "On Successfull Connection")
        self.socket.connectSocket()
        waitForExpectations(timeout: 100, handler: nil)
    }

    func socketDisConnect() {
        onDisconnect = self.expectation(description: "On Disconnect")
        self.socket.disconnect()
    }

    func onConnected() {
        XCTAssert(true, "Socket Connected Successfully")
        onSuccessfullSocketConnection.fulfill()
        print("TEST: onConnected")
    }

    func onDisconnected() {
        XCTAssert(true, "Socket Disconnected Successfully")
        onDisconnect.fulfill()
        print("TEST: onDisconnected")
    }

    func onReceiveText(message: Product<EnumResult>?) {
        if message == nil || message != nil {
            XCTAssert(true, "Socket did receive message successfully")
            print("TEST: onReceiveText")
            switch message?.t {
                case .connected:
                    socketDisConnect()
                default:
                    return
            }
        }
    }
}
