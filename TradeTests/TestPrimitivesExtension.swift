//
//  TestPrimitivesExtension.swift
//  TradeTests
//
//  Created by Yoganand Nandakumar on 06/05/2021.
//

import XCTest
@testable import Trade

class TestPrimitivesExtension: XCTestCase {
    func testToDoubleValue() {
        let str = "23.34"
        XCTAssertTrue(str.toDouble() == 23.34)
    }

    func testToStringValue() {
        let double = 23.34
        XCTAssertTrue(double.toString() == "23.34")
    }

    func testRoundedTo() {
        let double = 23.3445
        XCTAssertTrue(double.roundedTo(2) == "23.34")
    }
}

