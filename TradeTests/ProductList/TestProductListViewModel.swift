//
//  TestProductListViewModel.swift
//  TradeTests
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import XCTest
@testable import Trade

class TestProductListViewModel: XCTestCase {
    var viewModel: ProductListViewModel!
    var worker: ProductListWorkerProtocol!
    override func setUpWithError() throws {
        worker = ProductListWorker(networkManager: MockNetworkManager())
        viewModel = ProductListViewModel(worker: worker)
        testFetchProductList()
    }

    func testFetchProductList() {
        viewModel.fetchProductList()
        XCTAssertTrue(viewModel.productList != nil)
    }

    func testProductCount(){
        XCTAssertTrue(viewModel.productCount() == 2)
    }

    func testProductAtIndex() {
        let product = viewModel.productAtIndex(0)
        XCTAssertTrue(product != nil)
        XCTAssertTrue(product?.securityID == "sb42723")
        XCTAssertTrue(product?.displayName == "China A50")
    }

    override func tearDownWithError() throws {
        viewModel = nil
        worker = nil
    }
}
