//
//  TestProductPresenter.swift
//  TradeTests
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import XCTest
@testable import Trade

class TestProductPresenter: XCTestCase {
    var presenter1: ProductListPresenter!
    var presenter2: ProductListPresenter!
    override func setUpWithError() throws {
        if let product = StubResponse.responseFromFile("ProductStub",
                                                       toModel: Products.self) {
            presenter1 = ProductListPresenter(product: product[0])
            presenter2 = ProductListPresenter(product: product[1])
        }
    }

    func testReturnPrizeDifference() {
        XCTAssertTrue(presenter1 != nil)
        XCTAssertTrue(presenter1.returnPrizeDifference(currentPrize: 2800.0) == -28.915968519928914)
    }

    func testPresenter() {
        // Presenter1
        XCTAssertTrue(presenter1 != nil)
        XCTAssertTrue(presenter1.productName == "China A50")
        XCTAssertTrue(presenter1.currentPrize == "4004")
        XCTAssertTrue(presenter1.prizeDifference == 1.65016501650165)
        XCTAssertTrue(presenter1.prizeDifferenceOnPercent == "1.65%")
        XCTAssertTrue(!presenter1.isClosed)

        // Presenter2
        XCTAssertTrue(presenter2 != nil)
        XCTAssertTrue(presenter2.prizeDifference == -1.6233766233766231)
        XCTAssertTrue(presenter2.prizeDifferenceOnPercent == "-1.62%")
        XCTAssertTrue(presenter2.isClosed)
    }

    override func tearDownWithError() throws {
        presenter1 = nil
        presenter2 = nil
    }
}
