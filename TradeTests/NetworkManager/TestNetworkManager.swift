//
//  TestNetworkManager.swift
//  TradeTests
//
//  Created by Yoganand Nandakumar on 06/05/2021.
//

import XCTest
@testable import Trade

class TestNetworkManager: XCTestCase {
    var networkManager: NetworkManager!
    override func setUpWithError() throws {
        networkManager = NetworkManager()
    }

    /// Test Network Request with server data
    func testNetworkRequest() {
        let expectation = self.expectation(description: "On Success Network Request")
        let router = ProductTarget.getproduct
        networkManager.request(router: router,
                                   mapToSuccessModel: Products.self,
                                   success: { (response) in
                                    expectation.fulfill()                                    
                                    XCTAssertTrue(response.count > 0)
                                   }) { (error) in
            XCTFail()
        }
        waitForExpectations(timeout: 4, handler: nil)
    }

    override func tearDownWithError() throws {
        networkManager = nil
    }
}
