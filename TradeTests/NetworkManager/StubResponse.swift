//
//  StubResponse.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 03/05/2021.
//

import UIKit
/// Stubs response conversion from JSON to Data and JSON to String
final class StubResponse {
    static func fromJSONFile(_ fileName: String) -> Data? {
        guard let path = Bundle(for: StubResponse.self).path(forResource: fileName, ofType: "json") else {
            fatalError("Invalid path for json file")
        }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            fatalError("Invalid data from json file")
        }
        return data
    }

    static func responseFromFile <T: Codable> (_ fileName: String, toModel: T.Type) -> T? {
        guard let data = StubResponse.fromJSONFile(fileName) else {
            fatalError("Invalid data from json file")
        }
        guard let jsonObj = try? JSONDecoder().decode(T.self, from: data) else {
            fatalError("Unable to decode data")
        }
        return jsonObj
    }
}
