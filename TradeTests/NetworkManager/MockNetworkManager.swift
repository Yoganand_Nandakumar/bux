//
//  TestNetworkManager.swift
//  TradeTests
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import Foundation
@testable import Trade

enum NetworkResponseError: String {
    case unableToDecode = "Unable to decode the response."
    case emptyStub = "Empty Sample data"
    case defaultError = "Something went wrong"
}

class MockNetworkManager: RequestProvider {

    /// Stub request call
    ///
    /// - Parameters:
    ///   - request: The `NetworkRequest`
    ///   - successModel: `Codable` model `<T>` to which the response should be mapped
    ///   - successHandler: Block to be executed when the request is successful. Returns the mapped response `<T>`
    ///   - failureHandler: Block to be executed on failure of the request
    func request<T>(router: NetworkRequest,
                    mapToSuccessModel: T.Type,
                    success: @escaping SuccessBlock<T>,
                    failure: @escaping FailureBlock) where T : Decodable, T : Encodable {
        /// Fetch data from stubs
        do {
            guard let sampleFile = router.stubFileName,
                  let sampleData = StubResponse.fromJSONFile(sampleFile)
            else {
                failure(Error(developerMessage: NetworkResponseError.emptyStub.rawValue))
                return
            }
            let jsonObj = try JSONDecoder().decode(T.self, from: sampleData)
            success(jsonObj)
        } catch {
            failure(Error(developerMessage: NetworkResponseError.unableToDecode.rawValue))
        }
    }
}
