//
//  TestSocketResponse.swift
//  TradeTests
//
//  Created by Yoganand Nandakumar on 13/05/2021.
//

import XCTest
@testable import Trade

class TestSocketResponse: XCTestCase {
    func testConnectedMapping() {
        if let connectedJson = StubResponse.responseFromFile("SocketConnectedResponse",
                                                             toModel: Product<EnumResult>.self) {
            switch connectedJson.t {
                case .connected:
                    if let _ = connectedJson.body?.get() as? ConnectConnected {
                        XCTAssert(true)
                    } else {
                        XCTAssert(false)
                    }
                default:
                    XCTAssert(false)
            }
        }
    }

    func testTradingQuoteMapping() {
        if let connectedJson = StubResponse.responseFromFile("SocketTradingResponse",
                                                             toModel: Product<EnumResult>.self) {
            switch connectedJson.t {
                case .tradingQuote:
                    if let _ = connectedJson.body?.get() as? TradingQuote {
                        XCTAssert(true)
                    } else {
                        XCTAssert(false)
                    }
                default:
                    XCTAssert(false)
            }
        }
    }

    func testFailureMapping() {
        if let connectedJson = StubResponse.responseFromFile("SocketFailedResponse",
                                                             toModel: Product<EnumResult>.self) {
            switch connectedJson.t {
                case .failed:
                    if let _ = connectedJson.body?.get() as? TradeError {
                        XCTAssert(true)
                    } else {
                        XCTAssert(false)
                    }
                default:
                    XCTAssert(false)
            }
        }
    }
}
