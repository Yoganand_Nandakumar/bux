//
//  ProductListDataSource.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import UIKit

protocol ProductListDataSourceProtocol: class {
    func didSelectRowAt(_ index: Int)
}

final class ProductListDataSource: TableViewDataSource {
    var viewModel: ProductListViewModel!
    weak var delegate: ProductListDataSourceProtocol!

    init(viewModel: ProductListViewModel,
         delegate: ProductListDataSourceProtocol) {
        self.viewModel = viewModel
        self.delegate = delegate
    }

    // MARK: - Override Method
    override func registerReusableViews(with tableView: UITableView) {
        tableView.register(cellTypes: [ProductListTableViewCell.self])
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.productCount()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell: ProductListTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        guard let productInfo = viewModel.productAtIndex(indexPath.row)
        else {
            return UITableViewCell()
        }
        tableViewCell.bindData(model: ProductListPresenter(product: productInfo))
        return tableViewCell
    }

    // MARK: - Delegate Method
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.didSelectRowAt(indexPath.row)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}
