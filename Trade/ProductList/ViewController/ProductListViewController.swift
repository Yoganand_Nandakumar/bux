//
//  ProductListViewController.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

final class ProductListViewController: BaseViewController, Storyboarded {

    var viewModel: ProductListViewModel!
    weak var coordinator: ProductListCoordinator?
    private var datasource: TableViewDataSource!
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        self.showLoader()
        self.viewModel.fetchProductList()
    }

    /// Setting DataSource for Tableview
    private func setupDataSource() {
        datasource = ProductListDataSource(viewModel: viewModel, delegate: self)
        datasource.configure(with: tableView)
    }
}

extension ProductListViewController: ProductListViewModelProtocol {
    func onSuccessProduct() {
        self.removeLoader()
        self.setupDataSource()
        self.tableView.reloadData()
    }

    func onFailureProduct(error: Error?) {
        // In Case of showing any error on UI, will have to remove loader
        debugPrint(error?.errorCode?.rawValue ?? "Something went wrong")
    }
}

// MARK: - ProductListDataSourceProtocol
extension ProductListViewController: ProductListDataSourceProtocol {
    /// On Select of the tableview cell, didSelectRowAt will be fired
    func didSelectRowAt(_ index: Int) {
        if let productInfo = self.viewModel.productAtIndex(index) {
            self.coordinator?.navigateToDetailController(selectedProduct: ProductListPresenter(product: productInfo))
        }
    }
}
