//
//  ProductListPresenter.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import Foundation

/// Used to provide only presenting info for the UI
struct ProductListPresenter {

    var productName: String {
        return product.displayName
    }

    var currentPrize: String {
        return product.currentPrice.amount.toDouble().roundedTo(product.currentPrice.decimals)
    }

    // Retuning double to check negative and positive value
    var prizeDifference: Double {
        return returnPrizeDifference(currentPrize: currentPrize.toDouble())
    }

    var prizeDifferenceOnPercent: String {
        return prizeDifference.roundedTo(2) + "%"
    }

    var isClosed: Bool {
        return product.productMarketStatus == .closed
    }

    var currency: String {
        return product.quoteCurrency
    }

    var productId: String {
        return product.securityID
    }

    var displayDecimal: Int {
        return product.displayDecimals
    }

    func returnPrizeDifference(currentPrize: Double) -> Double {
        let diff = currentPrize - product.closingPrice.amount.toDouble()
        return (diff/product.closingPrice.amount.toDouble()) * 100
    }

    fileprivate var product: ProductElement

    init(product: ProductElement) {
        self.product = product
    }
}
