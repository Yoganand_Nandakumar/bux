//
//  ProductListWorker.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import Foundation

protocol ProductListWorkerProtocol: class {
    func fetchProductList(success: @escaping (Products) -> Void,
                          failure: @escaping (Error?) -> Void)
}

/// Remote worker is used to fetch product list
final class ProductListWorker: ProductListWorkerProtocol {
    var networkManager: RequestProvider
    init(networkManager: RequestProvider) {
        self.networkManager = networkManager
    }
}

extension ProductListWorker {

    /// Fetching Product List from server file
    /// - Parameters:
    ///   - success: return `Products` model
    ///   - failure: return `Error` model
    func fetchProductList(success: @escaping (Products) -> Void,
                          failure: @escaping (Error?) -> Void) {
        let router = ProductTarget.getproduct
        self.networkManager.request(router: router,
                                    mapToSuccessModel: Products.self) { (response) in
            success(response)
        } failure: { (error) in
            failure(error)
        }
    }
}
