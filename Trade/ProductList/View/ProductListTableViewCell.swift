//
//  ProductListTableViewCell.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import UIKit

final class ProductListTableViewCell: UITableViewCell {
    @IBOutlet var productName: UILabel!
    @IBOutlet var currentPrize: UILabel!
    @IBOutlet var prizeDifference: UILabel!
    @IBOutlet var lock: UIImageView!
    @IBOutlet var bg: UIView!

    override func prepareForReuse() {
        super.prepareForReuse()
        self.productName.text = ""
        self.currentPrize.text = ""
        self.prizeDifference.text = ""
        self.lock.image = nil
    }

    func bindData(model: ProductListPresenter) {
        self.productName.text = model.productName
        self.currentPrize.text = model.currentPrize + " " + model.currency
        self.prizeDifference.text = model.prizeDifferenceOnPercent
        self.lock.isHidden = !model.isClosed
        // Color change for prizeDifference on negative and no-negative values
        if model.prizeDifference.sign == .minus {
            self.prizeDifference.textColor = UIColor.red
        } else {
            self.prizeDifference.textColor = UIColor.green
        }
    }
}
