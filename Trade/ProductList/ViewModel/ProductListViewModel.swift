//
//  ProductListViewModel.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import Foundation

protocol ProductListViewModelProtocol: class {
    func onSuccessProduct()
    func onFailureProduct(error: Error?)
}

final class ProductListViewModel {
    let worker: ProductListWorkerProtocol!
    var productList: Products?
    weak var delegate: ProductListViewModelProtocol?

    init(worker: ProductListWorkerProtocol,
         delegate: ProductListViewModelProtocol? = nil) {
        self.worker = worker
        self.delegate = delegate
    }

    /// Fetching Product List
    func fetchProductList() {
        self.worker.fetchProductList { (product) in
            self.productList = product
            self.delegate?.onSuccessProduct()
        } failure: { (error) in
            self.delegate?.onFailureProduct(error: error)
        }
    }

    /// Will return prduct array list count
    func productCount() -> Int {
        return productList?.count ?? 0
    }

    /// Will return `ProductElement` object at `index`
    func productAtIndex(_ index: Int) -> ProductElement? {
        return productList?[index]
    }
}
