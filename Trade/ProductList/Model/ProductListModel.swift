//
//  ProductListModel.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import Foundation

// MARK: - ProductElement
struct ProductElement: Codable {
    let symbol, displayName, securityID: String
    let quoteCurrency: String
    let displayDecimals: Int
    let currentPrice, closingPrice: Price
    let category: String
    let favorite: Bool
    let productMarketStatus: ProductMarketStatus
    let tags: [String]
    let localizedMainTag: LocalizedMainTag

    enum CodingKeys: String, CodingKey {
        case symbol, displayName
        case securityID = "securityId"
        case quoteCurrency, displayDecimals,
             currentPrice, closingPrice,
             category, favorite,
             productMarketStatus, tags, localizedMainTag
    }
}

// MARK: - Price
struct Price: Codable {
    let currency: String
    let decimals: Int
    let amount: String
}

// MARK: - Range
struct Range: Codable {
    let currency: String
    let decimals: Int
    let high, low: String
}

// MARK: - LocalizedMainTag
struct LocalizedMainTag: Codable {
    let id: String
    let name: String
}

enum ProductMarketStatus: String, Codable {
    case closed = "CLOSED"
    case productMarketStatusOPEN = "OPEN"
    case phoneOnly = "PHONE_ONLY"
}

typealias Products = [ProductElement]

// MARK: - Error
public struct Error: Codable {
    let message: String?
    let developerMessage: String
    let errorCode: ErrorCode?
    init(message: String? = nil, developerMessage: String, errorCode: ErrorCode? = nil) {
        self.message = message
        self.developerMessage = developerMessage
        self.errorCode = errorCode
    }
}

enum ErrorCode: String, Codable {
    case TRADING_002 = "Unexpected error"
    case AUTH_007 = "Access token is not valid"
    case AUTH_014 = "User does not have sufficient permissions to perform this action"
    case AUTH_009 = "Missing Authorization header"
    case AUTH_008 = "Access token is expired"
}
