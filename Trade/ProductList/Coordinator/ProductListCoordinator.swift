//
//  ProductListCoordinator.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

final class ProductListCoordinator: BaseCoordinator {
    var childCoordinators = [BaseCoordinator]()

    var navigationController: UINavigationController

    private lazy var worker: ProductListWorker = {
        return ProductListWorker(networkManager: NetworkManager())
    }()

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let viewController = ProductListViewController.instantiate(storyBoardtype: .main)
        viewController.viewModel = ProductListViewModel(worker: worker,
                                                        delegate: viewController)
        viewController.coordinator = self
        self.navigationController.viewControllers = [viewController]
    }
}

// MARK: - Navigation
extension ProductListCoordinator {
    func navigateToDetailController(selectedProduct: ProductListPresenter) {
        let coordinator = ProductDetailCoordinator(navigationController: navigationController,
                                                   selectedProduct: selectedProduct)
        coordinator.listCoordinator = self
        self.add(childCoordinator: coordinator)
        coordinator.start()
    }

    func onDismissDetailController(coordinator: BaseCoordinator) {
        self.remove(childCoordinator: coordinator)
    }
}
