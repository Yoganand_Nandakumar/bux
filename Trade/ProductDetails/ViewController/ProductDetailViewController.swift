//
//  ProductListViewController.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

final class ProductDetailViewController: RealTimeSocketBaseViewController, Storyboarded {

    var viewModel: ProductDetailViewModel!
    weak var coordinator: ProductDetailCoordinator?

    @IBOutlet var displayName: UILabel!
    @IBOutlet var currentPrize: UILabel!
    @IBOutlet var prizeDifference: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindData()
    }

    /// UI Binding
    func bindData() {
        self.displayName.text = self.viewModel.product.productName
        self.currentPrize.text = self.viewModel.product.currentPrize + " " + self.viewModel.product.currency
        self.prizeDifference.text = self.viewModel.product.prizeDifferenceOnPercent
    }

    @IBAction func dismissController(_ sender: UITapGestureRecognizer) {
        self.coordinator?.dismissController()
    }
}

// MARK: - ProductDetailViewModelProtocol
extension ProductDetailViewController: ProductDetailViewModelProtocol {
    func readyToReceiveMessage() {
        debugPrint("ProductDetailViewController: websocket is connected")
        self.viewModel.subscribeProduct()
    }

    func onDisconnected() {
        debugPrint("ProductDetailViewController: websocket is disconnected")
    }

    func onSuccessTradeQuote(updatedValue: TradingQuote) {
        self.currentPrize.text = updatedValue.currentPrice + " " + self.viewModel.product.currency
        let diff = self.viewModel.product.returnPrizeDifference(currentPrize: updatedValue.currentPrice.toDouble())
        self.prizeDifference.text = diff.roundedTo(2) + "%"
    }
}

extension ProductDetailViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // Used to protect the touch action on the subview of the controller
        return touch.view == gestureRecognizer.view
    }
}
