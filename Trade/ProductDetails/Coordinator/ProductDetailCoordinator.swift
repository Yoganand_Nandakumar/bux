//
//  ProductDetailCoordinator.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

final class ProductDetailCoordinator: BaseCoordinator {
    var childCoordinators = [BaseCoordinator]()
    var selectedProduct: ProductListPresenter!
    weak var listCoordinator: ProductListCoordinator?
    var navigationController: UINavigationController

    init(navigationController: UINavigationController,
         selectedProduct: ProductListPresenter) {
        self.navigationController = navigationController
        self.selectedProduct = selectedProduct
    }

    func start() {
        let viewController = ProductDetailViewController.instantiate(storyBoardtype: .main)
        viewController.viewModel = ProductDetailViewModel(product: selectedProduct,
                                                          delegate: viewController)
        viewController.coordinator = self
        self.navigationController.present(viewController, animated: true, completion: nil)
    }

    /// On Dismiss Detail Controller
    func dismissController() {
        self.listCoordinator?.onDismissDetailController(coordinator: self)
        self.navigationController.dismiss(animated: true)
    }
}
