//
//  ProductDetailViewModel.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import Foundation

protocol ProductDetailViewModelProtocol: class {
    func readyToReceiveMessage()
    func onDisconnected()
    func onSuccessTradeQuote(updatedValue: TradingQuote)
}

final class ProductDetailViewModel {
    weak var delegate: ProductDetailViewModelProtocol?
    var product: ProductListPresenter!
    init(product: ProductListPresenter,
         delegate: ProductDetailViewModelProtocol? = nil) {
        self.product = product
        self.delegate = delegate
        SocketManager.shared.delegate = self
    }

    /// Subcribe product format to send
    /// - Returns: `QuoteUpdate` model
    private func subscribeToModel() -> QuoteUpdate {
        let quoteMsg = QuoteUpdate(subscribeTo: ["trading.product.\(product.productId)"])
        return quoteMsg
    }

    /// Unsubscribe product format to send
    /// - Returns: `QuoteUpdate` model
    private func unsubscribeFromModel() -> QuoteUpdate {
        let quoteMsg = QuoteUpdate(unsubscribeFrom: ["trading.product.\(product.productId)"])
        return quoteMsg
    }

    func subscribeProduct() {
        SocketManager.shared.sendMessage(subscribeToModel().jsonString() ?? "")
    }
    func unsubscribeProduct() {
        SocketManager.shared.sendMessage(unsubscribeFromModel().jsonString() ?? "")
    }
}

// MARK: - SocketManagerProtocol
extension ProductDetailViewModel: SocketManagerProtocol {
    func onConnected() {
        debugPrint("ProductDetailViewModel: websocket is connected")
    }
    func onDisconnected() {
        debugPrint("ProductDetailViewModel: websocket is disconnected")
        self.delegate?.onDisconnected()
    }
    /// On Receiving Socket message
    /// - Parameter message: `Product<EnumResult>?`
    func onReceiveText(message: Product<EnumResult>?) {
        if let message = message {
            switch message.t {
            case .connected:
                self.delegate?.readyToReceiveMessage()
            case .tradingQuote:
                if let updatedValue = message.body?.get() as? TradingQuote,
                   updatedValue.securityID == product.productId {
                    self.delegate?.onSuccessTradeQuote(updatedValue: updatedValue)
                }
            case .failed:
                if let error = message.body?.get() as? TradeError {
                    debugPrint("\(error.developerMessage) : \(error.errorCode)")
                }
            }
        }
    }
}
