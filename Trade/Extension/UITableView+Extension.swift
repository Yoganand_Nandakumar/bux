//
//  UITableView+Extension.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

public extension UITableView {
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier,
                                             for: indexPath) as? T else {
            fatalError("Unable to Dequeue Reusable Table View Cell")
        }
        cell.accessibilityIdentifier = cell.className + "\(indexPath.section)_\(indexPath.row)"
        return cell
    }
    func register(cellType: UITableViewCell.Type, bundle: Bundle? = nil) {
        let className = cellType.className
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forCellReuseIdentifier: className)
    }
    func register(cellTypes: [UITableViewCell.Type], bundle: Bundle? = nil) {
        cellTypes.forEach { register(cellType: $0, bundle: bundle) }
    }
}

// MARK: - For header and footer
public extension UITableView {
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
        guard let cell = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("Unable to Dequeue Reusable HeaderView")
        }
        return cell
    }
    func registerHeaderFooterView(cellTypes: [UITableViewHeaderFooterView.Type],
                                  bundle: Bundle? = nil) {
        cellTypes.forEach { registerHeaderFooterView(cellType: $0, bundle: bundle) }
    }
    func registerHeaderFooterView(cellType: UITableViewHeaderFooterView.Type,
                                  bundle: Bundle? = nil) {
        let className = cellType.className
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forHeaderFooterViewReuseIdentifier: className)
    }
}
