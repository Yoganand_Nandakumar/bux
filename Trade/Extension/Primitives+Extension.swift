//
//  Primitives+Extension.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import Foundation

extension String {
    func toDouble() -> Double {
        return Double(self) ?? 0.0
    }
}
extension Double {
    func toString() -> String {
        return "\(self)"
    }
    func roundedTo(_ places: Int) -> String {
        return String(format: "%.\(places)f", self)
    }
}
