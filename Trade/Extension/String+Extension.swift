//
//  String+Extension.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 09/05/2021.
//

import Foundation

extension String {
    func toCodable<T: Codable>(mapToModel model: T.Type) -> T? {
        guard let data = self.data(using: .utf8) else { return nil }
        return data.toCodable(mapToModel: model)
    }
}
extension Data {
    func toCodable<T: Codable>(mapToModel model: T.Type) -> T? {
        guard let jsonObj = try? JSONDecoder().decode(T.self, from: self) else {
            debugPrint("Unable to decode data")
            return nil
        }
        return jsonObj
    }
}
