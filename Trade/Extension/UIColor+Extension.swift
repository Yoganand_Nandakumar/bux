//
//  UIColor+Extension.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import UIKit

extension UIColor {
    convenience init(_ red: Int, green: Int, blue: Int, alpha: CGFloat) {
        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0,
                  alpha: alpha)
    }
}
