//
//  UIViewController+Extension.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 12/05/2021.
//

import UIKit

typealias OKHandler = ((UIAlertAction?) -> Void)?
typealias CancelHandler = ((UIAlertAction?) -> Void)?
typealias CompletionHandler = (() -> Void)?

// MARK: - extension: UIViewController - For alert
extension UIViewController {
    func alert(title: String?,
               message: String?,
               showOk: Bool = true,
               showCancel: Bool = false,
               okMessage: String = "OK",
               okHandler: OKHandler = nil,
               cancelHandler: CancelHandler = nil,
               completion: CompletionHandler = nil) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        if showCancel {
            let alertAction = UIAlertAction(title: "Cancel",
                                            style: UIAlertAction.Style.default,
                                            handler: cancelHandler)
            alertController.addAction(alertAction)
        }
        if showOk {
            alertController.addAction(UIAlertAction(title: okMessage,
                                                    style: .default,
                                                    handler: okHandler))
        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: completion)
        }
    }
}
