//
//  RealTimeSocketBaseViewController.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 11/05/2021.
//

import UIKit
import ReachabilitySwift

class RealTimeSocketBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        registerNotificationCenter()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ReachabilityManager.shared.addListener(listener: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SocketManager.shared.connectSocket()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ReachabilityManager.shared.removeListener(listener: self)
        SocketManager.shared.disconnect()
    }

    func registerNotificationCenter() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(appMovedToBackground),
                                       name: UIApplication.didEnterBackgroundNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(appCameToForeground),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
    }

    @objc func appMovedToBackground() {
        debugPrint("BaseViewController: app enters background")
        SocketManager.shared.disconnect()
    }

    @objc func appCameToForeground() {
        print("BaseViewController: app enters foreground")
        SocketManager.shared.connectSocket()
    }
}

// MARK: - NetworkStatusListener
extension RealTimeSocketBaseViewController: NetworkStatusListener {
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        DispatchQueue.main.async {
            if status == .notReachable {
                SocketManager.shared.disconnect()
                self.alert(title: "Could Not Connect",
                           message: "Please check your internet connection and try again",
                           showOk: true)
            } else {
                SocketManager.shared.connectSocket()
            }
        }
    }
}
