//
//  TableViewDataSource.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

public protocol TableViewDataSourceProtocol: UITableViewDataSource, UITableViewDelegate {
    /// Number of sections in the data source
    var numberOfSections: Int { get }

    /// Performs initial configuration. Call this method after view loaded.
    func configure(with tableView: UITableView)

    /// Register cell, header, and footer views with a table view
    func registerReusableViews(with tableView: UITableView)

    /// The table view of a data source.
    var tableView: UITableView? { get }
}

class TableViewDataSource: NSObject, TableViewDataSourceProtocol {

    // MARK: UITableViewDataSource
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        fatalError("\(type(of: self)) must override \(#function)")
    }

    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        fatalError("\(type(of: self)) must override \(#function)")
    }

    open func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }

    func registerReusableViews(with tableView: UITableView) {
        fatalError("\(type(of: self)) must override \(#function)")
    }

    var numberOfSections: Int {
        return 1
    }

    public var tableView: UITableView? {
        if let tableView = self._tableView {
            return tableView
        }
        return UITableView()
    }

    public func configure(with tableView: UITableView) {
        tableView.dataSource = self
        tableView.delegate = self
        registerReusableViews(with: tableView)
    }

    private weak var _tableView: UITableView?
}
