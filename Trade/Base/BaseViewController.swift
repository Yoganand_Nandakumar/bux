//
//  BaseViewController.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 11/05/2021.
//

import UIKit
import ReachabilitySwift

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ReachabilityManager.shared.addListener(listener: self)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ReachabilityManager.shared.removeListener(listener: self)
    }
}

// MARK: - NetworkStatusListener
extension BaseViewController: NetworkStatusListener {
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        DispatchQueue.main.async {
            if status == .notReachable {
                self.alert(title: "Could Not Connect",
                           message: "Please check your internet connection and try again",
                           showOk: true)
            }
        }
    }
}
