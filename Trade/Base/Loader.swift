//
//  Loader.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 13/05/2021.
//

import UIKit

extension UIViewController {
    func showLoader() {
        let blurLoader = Loader(frame: self.view.frame)
        self.view.addSubview(blurLoader)
    }

    func removeLoader() {
        if let blurLoader = self.view.subviews.first(where: { $0 is Loader }) {
            blurLoader.removeFromSuperview()
        }
    }
}

final class Loader: UIView {
    lazy var blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = frame
        blurEffectView.alpha = 0.5

        // Will ensure the blurEffectView
        // is the same size as its parent view.
        blurEffectView.autoresizingMask = [
            .flexibleWidth, .flexibleHeight
        ]
        return blurEffectView
    }()

    lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        indicator.style = .large
        indicator.color = .white
        blurEffectView.contentView.addSubview(indicator)
        indicator.center = blurEffectView.contentView.center
        return indicator
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(blurEffectView)
        // Loader Animation
        activityIndicator.startAnimating()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
