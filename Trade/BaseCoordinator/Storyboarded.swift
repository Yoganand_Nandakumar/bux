//
//  Storyboarded.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

enum StoryBoardType: String {
    case main = "Main"
}
protocol Storyboarded {
    static func instantiate(storyBoardtype: StoryBoardType) -> Self
}
extension Storyboarded where Self: UIViewController {
    static func instantiate(storyBoardtype: StoryBoardType) -> Self {
        let className = String(describing: self)
        let storyboard = UIStoryboard(name: storyBoardtype.rawValue, bundle: Bundle.main)

        guard let classRef = storyboard.instantiateViewController(withIdentifier: className) as? Self
        else {
            fatalError("No Identifier with this ClassName: \(String(describing: self))")
        }
        return classRef
    }
}
