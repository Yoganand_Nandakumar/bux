//
//  BaseCoordinator.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import UIKit

// MARK: - BaseCoordinator
// Should be inherited for all ViewController's coordinator.
public protocol BaseCoordinator: class {
    var childCoordinators: [BaseCoordinator] { get set }
    var navigationController: UINavigationController { get set }
    func start()
}
extension BaseCoordinator {
    public func add(childCoordinator coordinator: BaseCoordinator) {
        self.childCoordinators.append(coordinator)
    }
    public func remove(childCoordinator coordinator: BaseCoordinator?) {
        self.childCoordinators = self.childCoordinators.filter { $0 !== coordinator }
    }
}
