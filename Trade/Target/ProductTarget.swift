//
//  ProductTarget.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//

import Foundation
import Alamofire

enum ProductTarget {
    case getproduct
}

extension ProductTarget: NetworkRequest {
    var parameters: GenericParams? {
        switch self {
        case .getproduct:
            return [:]
        }
    }
    var parameterEncoding: ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    var method: Alamofire.HTTPMethod {
        switch self {
        case .getproduct:
            return .get
        }
    }
    var path: String {
        switch self {
        case .getproduct:
            return "/core/23/products"
        }
    }
    var stubFileName: String? {
        switch self {
        case .getproduct:
            return "ProductStub"
        }
    }
}
