//
//  UIView+Extension.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 05/05/2021.
//

import UIKit

extension UIView {
    @IBInspectable public var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
