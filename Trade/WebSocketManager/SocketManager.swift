//
//  SocketManager.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 08/05/2021.
//

import Foundation
import Starscream

// MARK: - SocketManagerProtocol
protocol SocketManagerProtocol: class {
    func onConnected()
    func onDisconnected()
    func onReceiveText(message: Product<EnumResult>?)
}

// MARK: - SocketManager
final class SocketManager {
    static let shared = SocketManager()
    private (set) var socket: WebSocket!
    private init() {
        connectSocket()
    }
    weak var delegate: SocketManagerProtocol?

    /// Return URLRequest by binding with endpoints & headers
    /// - Parameter requestModel: `SocketRequestModel`
    /// - Returns: `URLRequest`
    private func urlRequest(requestModel: SocketRequestModel) -> URLRequest {
        var request = URLRequest(url: URL(string: requestModel.endPoint)!)
        request.timeoutInterval = 5
        if let headers = requestModel.headers {
            for header in headers {
                request.setValue(header.value,
                                 forHTTPHeaderField: header.field)
            }
        }
        return request
    }

    /// Initial socket connection
    /// - Parameter requestModel: Model which holds `header` and request `endpoints`
    func connectSocket(requestModel: SocketRequestModel = SocketRequestModel()) {
        let request = urlRequest(requestModel: requestModel)
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }

    /// Used to send message to get update from socket
    /// - Parameter message: `message` to sent
    func sendMessage(_ message: String) {
        socket.write(string: message)
    }

    /// Socket Disconnection
    func disconnect() {
        socket.disconnect()
    }
}

// MARK: - WebSocketDelegate
extension SocketManager: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            debugPrint("SocketManager: websocket is connected: \(headers)")
            self.delegate?.onConnected()
        case .disconnected(let reason, let code):
            debugPrint("SocketManager: websocket is disconnected: \(reason) with code: \(code)")
            self.delegate?.onDisconnected()
        case .text(let string):
            debugPrint("SocketManager: Received text: \(string))")
            let json = string.toCodable(mapToModel: Product<EnumResult>.self)
            self.delegate?.onReceiveText(message: json)
        case .binary(let data):
            debugPrint("SocketManager: Received data: \(data.count)")
            let json = data.toCodable(mapToModel: Product<EnumResult>.self)
            self.delegate?.onReceiveText(message: json)
        case .cancelled:
            debugPrint("SocketManager: websocket is cancelled")
            self.delegate?.onDisconnected()
        case .error(let error):
            debugPrint(error ?? "Something went wrong on socket handler")
        default:
            break
        }
    }
}
