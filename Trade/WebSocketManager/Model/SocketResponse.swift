//
//  SocketResponse.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 08/05/2021.
//
// swiftlint:disable identifier_name

import Foundation

// MARK: - Product
struct Product<T: Codable>: Codable {
    let t: SocketState
    let id: String
    let v: Int
    let body: T?
}

enum SocketState: String, Codable {
    case connected = "connect.connected"
    case failed = "connect.failed"
    case tradingQuote = "trading.quote"
}

// MARK: - ConnectConnected
struct ConnectConnected: Codable {
    let sessionID: String
    let time: Int
    let pop: Pop
    let clientVersion: String

    enum CodingKeys: String, CodingKey {
        case sessionID = "sessionId"
        case time, pop, clientVersion
    }
}

struct Pop: Codable {
    let clientID, sessionID: String

    enum CodingKeys: String, CodingKey {
        case clientID = "clientId"
        case sessionID = "sessionId"
    }
}

// MARK: - TradingQuote
struct TradingQuote: Codable {
    let securityID, currentPrice: String

    enum CodingKeys: String, CodingKey {
        case securityID = "securityId"
        case currentPrice
    }
}

// MARK: - TradeError
struct TradeError: Codable {
    let developerMessage: String
    let errorCode: String
}

// Used for binding dynamic JSON
enum EnumResult: Codable {
    case tradingQuote(TradingQuote)
    case connectConnected(ConnectConnected)
    case tradeError(TradeError)
    case unknown

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let body = try? container.decode(TradingQuote.self) {
            self = .tradingQuote(body)
            return
        }
        if let body = try? container.decode(ConnectConnected.self) {
            self = .connectConnected(body)
            return
        }
        if let body = try? container.decode(TradeError.self) {
            self = .tradeError(body)
            return
        }
        self = .unknown
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .tradingQuote(let body):
            try container.encode(body)
        case .connectConnected(let body):
            try container.encode(body)
        case .tradeError(let body):
            try container.encode(body)
        case .unknown:
            return
        }
    }
    func get() -> Any {
        switch self {
        case .tradingQuote(let body):
            return body
        case .connectConnected(let body):
            return body
        case .tradeError(let body):
            return body
        case .unknown:
            return ""
        }
    }
}
