//
//  SocketRequestModel.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 08/05/2021.
//

import Foundation

// MARK: - SocketRequestModel
struct SocketRequestModel {
    let endPoint: String
    var headers: [HTTPHeader]?

    init(endPoint: String = Constant.socketEndPoint,
         headers: [HTTPHeader]? = [("Accept", Constant.accept),
                                 ("Accept-Language", Constant.acceptLanguage),
                                 ("Authorization", Constant.authorication)]) {
        self.endPoint = endPoint
        self.headers = headers
    }
}

// MARK: - QuoteUpdate
struct QuoteUpdate: Codable {
    let subscribeTo, unsubscribeFrom: [String]?
    init(subscribeTo: [String]? = nil,
         unsubscribeFrom: [String]? = nil) {
        self.subscribeTo = subscribeTo
        self.unsubscribeFrom = unsubscribeFrom
    }
    func jsonString() -> String? {
        guard let encodedData = try? JSONEncoder().encode(self) else { return nil }
        let jsonString = String(data: encodedData,
                                encoding: .utf8)
        return jsonString
    }
}
