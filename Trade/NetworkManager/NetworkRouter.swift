//
//  NetworkRouter.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 03/05/2021.
//

import Alamofire

typealias NetworkRequest = Routable
public typealias JSONParams = [String: Any]
public typealias GenericParams = Any
public typealias HTTPHeader = (field: String, value: String)

public protocol Routable: URLRequestConvertible {
    var method: Alamofire.HTTPMethod { get }
    var endPoint: URL? { get }
    var parameters: GenericParams? { get }
    var headers: [HTTPHeader]? { get }
    var parameterEncoding: ParameterEncoding { get }
    var stubFileName: String? { get }
    var path: String { get }
}

extension Routable {
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        guard let url = endPoint
        else {
            throw AFError.invalidURL(url: "")
        }
        var  urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        if let headers = headers {
            for header in headers {
                urlRequest.setValue(header.value, forHTTPHeaderField: header.field)
            }
        }
        urlRequest = try parameterEncoding.encode(urlRequest, with: parameters as? [String: Any] )
        return urlRequest
    }

    var parameterEncoding: ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }

    var endPoint: URL? {
        return try? Constant.endPoint.asURL()
    }

    var headers: [HTTPHeader]? {
        return [("Accept", Constant.accept),
                ("Accept-Language", Constant.acceptLanguage),
                ("Authorization", Constant.authorication)
        ]
    }

    var stubFileName: String? {
        return nil
    }
}
