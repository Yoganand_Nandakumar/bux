//
//  NetworkManager.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 03/05/2021.
//

import Alamofire

/// Network response error
enum NetworkResponseError: String {
    case unableToDecode = "Unable to decode the response."
    case defaultError = "Something went wrong"
}

public typealias SuccessBlock<T> = (_ response: T) -> Void
public typealias FailureBlock = (_ error: Error?) -> Void

protocol RequestProvider {
    func request <T: Codable> (router: NetworkRequest,
                               mapToSuccessModel: T.Type,
                               success: @escaping SuccessBlock<T>,
                               failure: @escaping FailureBlock)
}

struct NetworkManager: RequestProvider {
    /// Fetching data on server
    /// - Parameters:
    ///   - router: Holds request information
    ///   - mapToSuccessModel: `Codable` model `<T>` to which the response should be mapped
    ///   - success: To be executed when the request is successfull. Returns the mapped response `<T>`
    ///   - failure: To be executed on failure of the request
    func request <T: Codable> (router: NetworkRequest,
                               mapToSuccessModel: T.Type,
                               success: @escaping SuccessBlock<T>,
                               failure: @escaping FailureBlock) {
        Alamofire.request(router)
            .validate()
            .responseData { (response) in
                switch (response.result) {
                case .success(let data):
                    do {
                        let jsonObj = try JSONDecoder().decode(T.self, from: data)
                        success(jsonObj)
                    } catch {
                        print(error.localizedDescription)
                        failure(Error(developerMessage: NetworkResponseError.unableToDecode.rawValue))
                    }
                case .failure(let error):
                    failure(Error(developerMessage: error.localizedDescription))
                }
            }
    }
}
