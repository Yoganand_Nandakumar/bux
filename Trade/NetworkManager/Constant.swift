//
//  Constant.swift
//  Trade
//
//  Created by Yoganand Nandakumar on 04/05/2021.
//
// swiftlint:disable line_length

class Constant {
    static let authorication = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJyZWZyZXNoYWJsZSI6ZmFsc2UsInN1YiI6ImJiMGNkYTJiLWExMGUtNGVkMy1hZDVhLTBmODJiNGMxNTJjNCIsImF1ZCI6ImJldGEuZ2V0YnV4LmNvbSIsInNjcCI6WyJhcHA6bG9naW4iLCJydGY6bG9naW4iXSwiZXhwIjoxODIwODQ5Mjc5LCJpYXQiOjE1MDU0ODkyNzksImp0aSI6ImI3MzlmYjgwLTM1NzUtNGIwMS04NzUxLTMzZDFhNGRjOGY5MiIsImNpZCI6Ijg0NzM2MjI5MzkifQ.M5oANIi2nBtSfIfhyUMqJnex-JYg6Sm92KPYaUL9GKg"
    static let endPoint = "https://api.beta.getbux.com"

    static let socketEndPoint = "https://rtf.beta.getbux.com/subscriptions/me"
    static let accept = "application/json"
    static let acceptLanguage = "nl-NL,en;q=0.8"
}
