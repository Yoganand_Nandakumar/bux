

### Trade (BUX) - Requirement
	
 The main goal is to build a very simple client app that does the following:
 
- In a first screen, obtain, from the user input or from a list of pre-defined items, a Product Identifier; what we call a "Product" is a Trading product, such as a stock from Apple or Facebook.
- In a second screen, show the data of the Product with the provided Identifier. To show current price of the Product, please take into account the right decimal digits and locale-aware currency formatting. Besides the value of the previous day closing price, please also show the difference of the previous day closing price to the current price, in %. (e.g. if previous day = $100,00 and current = $150,00, the % difference will be 50%).
- Subscribe to the real-time feed channel of this Product.
- Update the information shown according to the WebSocket update messages (i.e. updated price value and previous day % difference). Remember to also properly handle common scenarios and errors from the nature of a mobile app, such as an unstable mobile connection, app switching, disconnection, etc. It's up to you to decide what to do in these cases.

## Content

- Architecture
- Pods
- How to run
- Unit test

## Architecture

 - MVVM with Coordinator, Presenter & Worker
 - Network layer for Alamofire with Protocol-Oriented Approach

## Pods

- **Alamofire** - For Networking
- **ReachabilitySwift** - Handling Offline State
- **Starscream** - For WebSocket

## How to run

   To run the project just open Trade.xcworkspace and click on the run button. No pre-configuration required to run the project

## Unit test

   Written Unit Test cases for ProductListViewModel(TestProductListViewModel), ProductListPresenter(TestProductPresenter), PrimitivesExtension(TestPrimitivesExtension), NetworkManager(TestNetworkManager)
TestSocketResponse and SocketManager(TestSocketManager) 



